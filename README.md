# nuxt-website-lock

[![npm version][npm-version-src]][npm-version-href]
[![npm downloads][npm-downloads-src]][npm-downloads-href]
[![Github Actions CI][github-actions-ci-src]][github-actions-ci-href]
[![Codecov][codecov-src]][codecov-href]
[![License][license-src]][license-href]

> 

[📖 **Release Notes**](./CHANGELOG.md)

## Setup

1. Add `nuxt-website-lock` dependency to your project

```bash
yarn add nuxt-website-lock # or npm install nuxt-website-lock
```

2. Add `nuxt-website-lock` to the `modules` section of `nuxt.config.js`

```js
{
  modules: [
    // Simple usage
    'nuxt-website-lock',

    // With options
    ['nuxt-website-lock', { /* module options */ }]
  ]
}
```

## Development

1. Clone this repository
2. Install dependencies using `yarn install` or `npm install`
3. Start development server using `npm run dev`

## License

[MIT License](./LICENSE)

Copyright (c) 

<!-- Badges -->
[npm-version-src]: https://img.shields.io/npm/v/nuxt-website-lock/latest.svg
[npm-version-href]: https://npmjs.com/package/nuxt-website-lock

[npm-downloads-src]: https://img.shields.io/npm/dt/nuxt-website-lock.svg
[npm-downloads-href]: https://npmjs.com/package/nuxt-website-lock

[github-actions-ci-src]: https://github.com//workflows/ci/badge.svg
[github-actions-ci-href]: https://github.com//actions?query=workflow%3Aci

[codecov-src]: https://img.shields.io/codecov/c/github/.svg
[codecov-href]: https://codecov.io/gh/

[license-src]: https://img.shields.io/npm/l/nuxt-website-lock.svg
[license-href]: https://npmjs.com/package/nuxt-website-lock
